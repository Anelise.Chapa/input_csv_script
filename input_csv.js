#!/usr/bin/env node
/***********************************************************
* input_csv.js
* Author: Anelise Chapa 
************************************************************
* Description: 
* 
************************************************************/

/***********************************************************
* Constant definitions
************************************************************/

//node packages
const fs = require("fs");
const fs2 = require("fs");
const readline = require("readline");
const { parse } = require("csv-parse");
const encoding = require("encoding");
const { stringify } = require('querystring');
const { exit } = require('process');

const READ_ONLY_ACCESS = "READ_ONLY"
const READ_WRITE_ACCESS = "READ_WRITE"

/***********************************************************
* Global variable definitions
************************************************************/

let dataType            = []
let dataName            = []
let group               = []
let tsName              = []
let webName             = []
let descriptiveText     = []
let id                  = []
let source              = []
let access              = []
let maxInstances        = []
let simDefault          = []
let offset              = []
let base                = []
let exponent            = []
let minValue            = []
let maxValue            = []
let nativeUnit          = []
let listArray           = []
let minLength           = []
let maxLength           = []
let displayFormat       = []
let imbReadFunction     = []
let imbStartAddress     = []
let imbNumAddresses     = []
let imbSectionStart     = []
let my_imbSectionStart  = []
let snmpOID             = []
var input_csv_index     = 0;

/***********************************************************
* function definitions
************************************************************/

/***********************************************************
* Main program
************************************************************/

fs.createReadStream("./data_dictionary.csv")
  .pipe(parse({ delimiter: "\n", from_line: 2 }))
  .on("data", function (row) {
    //
    //input info as columns
    //
    var row_as_string = row.toString()
    data_in = row_as_string.split(',')
    dataType.push(data_in[0])
    dataName.push(data_in[1])
    group.push(data_in[2])
    tsName.push(data_in[3])
    webName.push(data_in[4])
    descriptiveText.push(data_in[5])
    id.push(data_in[6])
    source.push(data_in[7])
    access.push(data_in[8])
    maxInstances.push(data_in[9])
    simDefault.push(data_in[10])
    offset.push(data_in[11])
    base.push(data_in[12])
    exponent.push(data_in[13])
    minValue.push(data_in[14])
    maxValue.push(data_in[15])
    nativeUnit.push(data_in[16])
    listArray.push(data_in[17])
    displayFormat.push(data_in[20])
    imbReadFunction.push(data_in[21])
    imbSectionStart.push(data_in[24])
    snmpOID.push(data_in[25])

    //
    //After input, calculate output columns
    //
    //Calculate number of 16 bit address spaces needed based on data type
    //
    if(dataType[input_csv_index] == "NUMERIC") 
    {
      imbNumAddresses.push(Number(1))
      minLength.push(0)
      maxLength.push(0)
    } 
    else if(dataType[input_csv_index] == "STRING")
    {
      let string_len = simDefault[input_csv_index].toString().length
      minLength.push(string_len)
      maxLength.push(string_len)
      imbNumAddresses.push(Number(string_len)   )
    } 
    else if(dataType[input_csv_index] == "BYTE_BUFFER")
    {
      var byte_buffer_length = simDefault[input_csv_index].toString().split(" ").length
      imbNumAddresses.push(Number(byte_buffer_length))
      minLength.push(0)
      maxLength.push(0)
    } 
    else if(dataType[input_csv_index] == "LIST")
    {
      var list = listArray[input_csv_index].toString().split("'")
      var list_length = 0
      for(let index = 0; index < list.length; index++)
      {
        if(list[index] == ' ' || list[index] == '['|| list[index] == ']') 
        {

        }
        else {
          list_length++;
        }
      }
      minLength.push(0)
      maxLength.push(0)
      imbNumAddresses.push(Number(list_length))
    }

    //
    //Calculate Start Address based on last read value with same type of access
    //READ_ONLY or READ_WRITE
    //Start Address = last_section_start + number_of_addresses used by last value with same access
    //Only exception: If SectionStart is not empty, Start Address = Section Start
    //
    //Check sectionStart first if not empty
    //WARNING: WILL ONLY WORK IF FIRST POSITION IN SECTION START IS NOT EMPTY FOR EVERY ACCESS TYPE
    if(imbSectionStart[input_csv_index] != '')
    {
      imbStartAddress.push(Number(imbSectionStart[input_csv_index]))
    } 
    //Look for last access of same type
    else 
    {
      for(let index = input_csv_index -1; index >= 0; index--) 
      {
        //found last access of the same type
        if(access[index] == access[input_csv_index]) 
        {
          imbStartAddress.push(Number(imbStartAddress[index]) + Number(imbNumAddresses[index]))
          //Run only once
          break;
        }
      } 
    }
    input_csv_index++;


  })
  .on("end", function () {
    //
    //Write to output csv
    //
    const writeStream = fs2.createWriteStream('output.csv');
    writeStream.write("dataType,dataName,group,tsName,webName,descriptiveText,id,source,access,maxInstances,simDefault,offset,base,exponent,minValue,maxValue,nativeUnit,listArray,minLength,maxLength,displayFormat,imbReadFunction,imbStartAddress,imbNumAddresses,imbSectionStart,snmpOID\n")
    for(let index = 0; index < dataType.length; index++)
    {
      writeStream.write(
        dataType[index] + ',' +
        dataName[index] + ',' +
        group[index] + ',' +
        tsName[index] + ',' +
        webName[index] + ',' +
        descriptiveText[index] + ',' +
        id[index] + ',' +
        source[index] + ',' +
        access[index] + ',' +
        maxInstances[index] + ',' +
        simDefault[index] + ',' +
        offset[index] + ',' +
        base[index] + ',' +
        exponent[index] + ',' +
        minValue[index] + ',' +
        maxValue[index] + ',' +
        nativeUnit[index] + ',' +
        listArray[index] + ',' +
        minLength[index] + ',' +
        maxLength[index] + ',' +
        displayFormat[index] + ',' +
        imbReadFunction[index] + ',' +
        imbStartAddress[index] + ',' +
        imbNumAddresses[index] + ',' +
        imbSectionStart[index] + ',' +
        snmpOID[index] + '\n' );
    }
    console.log("Done!")
  })
  .on("error", function (error) {
    console.log(error.message);
  });

/***********************************************************
* EOF
************************************************************/